const dotenv = require('dotenv')
dotenv.config()

const config = {
  uri: process.env.URI,
  port: process.env.APP_PORT,
}

module.exports = config
