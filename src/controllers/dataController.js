const { findRecords } = require('../database');

module.exports = {
    getRecords: async (req, res) => {
        try {
            const filter = {
                createdAt: { $gte: new Date(req.body.startDate), $lt: new Date(req.body.endDate) },
                // counts: { $gte: [{ $sum: "$counts" }, 1000] }
            }
            const abc = await findRecords(filter);
            const records = [];
            abc.forEach((rcrd) => {
                const sum = rcrd.counts.reduce((partialSum, a) => partialSum + a, 0);
                if (sum > req.body.minCount && sum < req.body.maxCount ) {
                    records.push({
                        key: rcrd.key,
                        createdAt: rcrd.createdAt,
                        totalCount: sum,
                    })
                }
            })
            return {
                code: 0,
                msg: 'success',
                records
            }
        } catch (error) {
            return error
        }
    }
}   