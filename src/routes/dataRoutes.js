const router = require('express').Router();
const { dataController } = require('../controllers');

router.post('/find', (req, res, next) => {
    dataController.getRecords(req)
    .then(next)
    .catch(next);
})

module.exports = router;