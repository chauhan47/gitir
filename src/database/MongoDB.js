const { MongoClient } = require('mongodb');
const config = require('../../config');

class MongoDB {
    constructor(props) {
        this.client = new MongoClient(config.uri);
        this.database = props.database;
    }

    getConnection() {
        if (!this.connection) {
            throw new Error(`${this.database} connection is not present.`);
        }
        return this.connection;
    }
    connect() {
        try {
            return await this.client.connect();
        } catch (e) {
            console.error("connnection err", e);
        }
        finally {
            await client.close();
        }
    }
}

module.exports = MongoDB;
